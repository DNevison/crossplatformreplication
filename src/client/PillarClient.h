#ifndef PILLAR_CLIENT_H
#define PILLAR_CLIENT_H

#include "Pillar.h"
#include "SpriteComponent.h"

class PillarClient : public Pillar
{
public:
	static	GameObjectPtr	StaticCreate()  { return GameObjectPtr( new PillarClient() ); }

	virtual void Update();
	virtual void	HandleDying() override;

	virtual void	Read( InputMemoryBitStream& inInputStream ) override;

protected:
	PillarClient();


private:
	float				mTimeLocationBecameOutOfSync;
	float				mTimeVelocityBecameOutOfSync;

	SpriteComponentPtr	mSpriteComponent;
};
typedef shared_ptr< PillarClient >	PillarClientPtr;
#endif //PILLAR_CLIENT_H
